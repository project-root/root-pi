# README #

### rooT System  - 0.1 ###

* There are three parts to this system:

    * Arduino
    
        * Arduinos constitute the sensors and nodes. They mostly forward data to the master, but can also recieve commands from the master (in the case of an actuator). 

    * Rasberry Pi
    
        * The Rasberry Pi acts as the master for the mesh network. There are two concurrent processes that talk to each other, a Mesh Layer and an Internet Layer. The mesh layer reads data from the nodes, and the internet layer relays this information to the cloud.

    * Web
    
        * The Cloud sever runs a MongoDB instance that stores all the data. The data can be viewed on a HTML5 app.

### How do I get set up? ###

* Arduino
    
    * First make sure you have the RF24 library installed - https://github.com/tmrh20/RF24
    * You also need a printf.h file in the libarary directory. Just get this from the interwebs.
    * Annnd thats it :)

* Rasberry Pi
    
    * First you need Pthon3 
    * Then you need to make sure SPI and GPIO are enabled - http://raspi.tv/how-to-enable-spi-on-the-raspberry-pi
    * Then make sure you install the Requests library
    * Then install Cython for Python3 (will take a LOT of time)
    * Then install the Optimized RPi RF24 library - https://github.com/tmrh20/RF24
    * Then install the pyRF24 driver (Use the modified version that is included!) - https://github.com/newxan/blue_house/tree/master/RF24/pyRF24
    
* Web
    
    * Install PHP, MongoDB, Nginx, Etc

### How to use ###

1. Make sure the Server is running
1. Connect the RPi to the internet
1. On the RPi, run 'sudo python3 Local_Server.py'
1. Place arduino's in the field and turn them on. Recomened not to move them after turning them on.
1. Visit the website to see the data (currently hard-coded to a certain account)