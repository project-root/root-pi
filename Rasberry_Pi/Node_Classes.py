import json

class Area:

    def __init__(self, name):
        self.name = name
        self.idealMoistureLevel = 50
        self._actuatorState = 'Closed'
        self.nodeList = []
        self.changed = False
        self.override = False
        #self.override_moisture = 50
        
    def addNode(self, newNode):
        self.nodeList.append(newNode)
    
    def removeNode(self, delNodeID):
        #remove objects by creating a new list comprehension
        self.nodeList[:] = [item for item in self.nodeList if not item.nodeID == delNodeID]
    
    def numNodes(self):
        return len(self.nodeList)
        
    #Get a Dict of this Object
    def getDict(self):
        
        #add all the nodes
        _nodes = {}
        for item in self.nodeList:
            _nodes[item.nodeID] = item.getDict()
        
        #generate the output
        _output = {'Area_Name':self.name,'Area_Actuator_Status':self._actuatorState,'Nodes':_nodes}
        
        return _output
        
    #Get a Dict of this Object's nodes
    def getDictofNodes(self):
        
        #add all the nodes
        _nodes = {}
        for item in self.nodeList:
            _nodes[item.nodeID] = item.getDict()
        
        return _nodes
    
    #Get a JSON string of this Object's Nodes
    def getJSONofNodes(self):
        return json.dumps(self.getDictofNodes())
        
    #Get a JSON string of this Object
    def getJSON(self):
        return json.dumps(self.getDict())
    
    #Search for a node
    def hasNode(self, searchNodeID):
        for item in self.nodeList:
            if (item.nodeID == searchNodeID):
                return True
        return False
    
    #Search for the node, and set the value. The object takes care of error detection
    def setNodeValue(self, searchNodeID, newValue):
        for item in self.nodeList:
            if (item.nodeID == str(searchNodeID)):
                item.currentValue = newValue
                
    def getNode(self, searchNodeID):
        for item in self.nodeList:
            if (item.nodeID == searchNodeID):
                return item
    
    def _getActuatorState(self):
        return self._actuatorState
        
    def _setActuatorState(self, newState):
        for item in self.nodeList:
            #check if the node is a moisture sensor
            if(isinstance(item,Actuator)):
                item.currentValue = newState
        self._actuatorState = newState
        
    actuatorState = property(_getActuatorState,_setActuatorState)
    
class NetworkNode:
    
    def __init__(self, nodeID):
        #The Node ID, for identification
        self.nodeID = str(nodeID)
    
    #get the Dict rep of this object
    def getDict(self):
        return {'NodeID':self.nodeID}
        
    #get the JSON rep of this object
    def getJSON(self):
        return json.dumps(self.getDict())
    
    def __str__(self):
        return self.nodeID

class Actuator(NetworkNode):

    def __init__(self,nodeID,):
        #set initial state to closed
        self._state = 'Closed'
        
        super().__init__(nodeID)
        
    #get the JSON rep of this object
    def getDict(self):
        return {'NodeID':self.nodeID,'Type':'Actuator','State':self._state}
    
    def _getState(self):
        return self._state
        
    def _setState(self, newState):
        if(newState == 'Opened' or newState =='1'):
            #----insert code here to open actuator ------
            #self.radioObj.writeToAll('&'+self.nodeID+'1');
            #--------------------------------------------
            self._state = 'Opened'
        elif(newState == 'Closed' or newState =='0'  or newState ==''):
            #----insert code here to close actuator ------
            #self.radioObj.writeToAll('&'+self.nodeID+'0');
            #--------------------------------------------
            self._state = 'Closed'
        else:
            #uhh that's basically all an actuator can do (for now)
            raise ValueError('An Actuator can only be Opened or Closed, it cannot be %s' % (newState))
            
    currentValue = property(_getState,_setState)

class Sensor(NetworkNode):
    
    def __init__(self,nodeID):
        #Number of Data Points collected
        self._n = 0
        
        #The latest sensor reading
        self._currentValue = 0
        
        #Unit Measurement
        self._units = ''
        
        super().__init__(nodeID)
    
    #get dictionary rep of this object
    def getDict(self):
        return {'NodeID':self.nodeID,'Type':'Sensor','Values':self._currentValue}

    
    #Current Value--------------------------------------------
    
    def _getCurrentValue(self):
        return self._currentValue
        
    def _setCurrentValue(self, newValue):
        
        self._currentValue = newValue
        
        
    currentValue = property(_getCurrentValue,_setCurrentValue)

class MoistureSensor(Sensor):

    def __init__(self,nodeID):
        #call parent method
        super().__init__(nodeID)
        
        #Max Voltage for the Sensor (i.e 100% wet)
        self._maxVoltage = 1024
        self._minVoltage = 0
        self._units = '%'
    
    def _getCurrentValue(self):
        return self._currentValue

    def _setCurrentValue(self, outputs):
        outputs['Moisture'] = int(outputs['Moisture'])
        outputs['Ground Temp'] = float(outputs['Ground Temp'])
        outputs['Air Temp'] = float(outputs['Air Temp'])
        outputs['Humidity'] = float(outputs['Humidity'])
        super(MoistureSensor, self)._setCurrentValue(outputs)

    currentValue = property(_getCurrentValue,_setCurrentValue)
    
       
'''
#Test Code

N = NetworkNode('Node')
print(N.nodeID)

S = Sensor('Sensor')
print(S.nodeID)
print(S.currentValue)
print(S.averageValue)
print(S.movingAverageValue)
S.currentValue = 10
print(S.currentValue)
print(S.averageValue)
print(S.movingAverageValue)

S.currentValue = 20
S.currentValue = 30
S.currentValue = 40
S.currentValue = 40
S.currentValue = 40
print(S.currentValue)
print(S.averageValue)
print(S.movingAverageValue)

M = MoistureSensor('Moisture Sensor')
print(M.nodeID)
print(M.currentValue)
print(M.averageValue)
print(M.movingAverageValue)
M.currentValue = 5
print(M.currentValue)
print(M.averageValue)
print(M.movingAverageValue)

M.currentValue = 3
M.currentValue = 2
M.currentValue = 5
print(M.currentValue)
print(M.averageValue)
print(M.movingAverageValue)

A = Actuator('Actuator')
print(A.nodeID)
print(A.currentValue)
A.currentValue = 'Opened'
print(A.currentValue)
A.currentValue = 'Closed'
print(A.currentValue)
#A.currentValue = 'lolcats'
#print(A.currentValue)

Area = Area('Area 1')
print(Area.name)
Area.addNode(M)
Area.addNode(A)
Area.addNode(S)
print('[%s]' % ', '.join(map(str, Area.nodeList)))
print(Area.hasNode('Sensor'))
print(Area.hasNode('Sensor2'))
Area.removeNode('Sensor')
print('[%s]' % ', '.join(map(str, Area.nodeList)))

print(Area.moistureLevel('Current'))
print(Area.moistureLevel('Average'))
print(Area.moistureLevel('Moving Average'))

Area.setNodeValue('Moisture Sensor', 2)

print(Area.moistureLevel('Current'))
print(Area.moistureLevel('Average'))
print(Area.moistureLevel('Moving Average'))

Area.setNodeValue('Actuator', 'Opened')

Area.setNodeValue('Actuator', 'Opened')
'''
