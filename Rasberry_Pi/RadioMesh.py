from pyRF24 import pyRF24
import time
import json

class RadioMesh:

    def __init__(self, Node_Values, Sensor_Time):
        
        #Set up Radio
        self.Node_Values = Node_Values;
        self.radio = pyRF24("/dev/spidev0.0", 8000000, 25, retries = (15,15), dataRate = 2, autoAck = True, crcLength = 2)
        self.radio.enableAckPayload()
        self.selfID = 0xF0F0F0F0
        self.openChan = 0xF0F0F0F0A0
        self.newChan = 0xF0F0F0F0A1
        
        self.childDepthInfo = 1
        self.depthInfo = 2
        self.masterInfo = 3
        self.commandInfo = 4
        self.checkChildPathInfo = 5
        self.checkPathInfo = 6
        self.imOn = 9
        
        self.actuator  = 1
        self.sensor = 2
        
        self.depth = 0
        self.read_child = [self.selfID+1,self.selfID+2,self.selfID+3,self.selfID+4,self.selfID+5]
        self.write_child = ["","","","",""]
        self.check_child = [False,False,False,False,False]
        
        self.numChildren = 0
        self.maxChildren = 5
        
        self.sensorTime = Sensor_Time
        self.checkTime = 0
        self.checkInterval = self.sensorTime * 2
        
        self.radio.openReadingPipe(0,self.openChan)
        self.radio.openReadingPipe(1,self.read_child[0])
        self.radio.openReadingPipe(2,self.read_child[1])
        self.radio.openReadingPipe(3,self.read_child[2])
        self.radio.openReadingPipe(4,self.read_child[3])
        self.radio.openReadingPipe(5,self.read_child[4])
        self.radio.startListening()
        
        while True:
            #ping to open
            if(self.numChildren < self.maxChildren):
                output = self.imOn
                self.writeOut(self.newChan,output)

            time.sleep(1)

       
            #While loop to process messages
            while(self.radio.available()):
                message = self.radio.read(24)
                
                print(hex(int.from_bytes(message, byteorder='big', signed=False)));
                
                header = int.from_bytes(message[:1], byteorder='big', signed=False)
                #print("Rec Header: " + str(header))
                
                TalkBackID = int.from_bytes(message[1:5], byteorder='big', signed=False)
                #print("Message NodeID:  " + str(TalkBackID))
                
                #Get the pipe to write back to the child
                talkBack = TalkBackID << 8
                
                #Get the write slot of the child
                try:
                    writeSlot = self.write_child.index(talkBack)
                except ValueError:
                    writeSlot = -1

###########################################################################################################
                #///////////////////////////////////////////////////////////////////////
                #// Potential child is asking for depth
                #//
                #// Packet Structure = B       BBBB
                #//                    Header  NodeID (Pipe)
                #///////////////////////////////////////////////////////////////////////
            
                if(header == self.childDepthInfo and self.numChildren < self.maxChildren):
                    #-------------------------------------------------------------------------

                    #- 2 = depth info
                    #-
                    #- Send the child the depth info of this node
                    #-
                    #- Packet Structure = B       BBBB     BBBBB         B      B
                    #-                    Header  NodeID   ParentPipeID  Depth  Sensor Interval 
                    #-
                    #------------------------------------------------------------------------
                    
                    #header
                    output = self.depthInfo
                    
                    #find empty slot
                    try:
                        slot = self.write_child.index("")
                    except ValueError:
                        slot = -1
                            
                    #Pipe ID
                    output = output << 32 
                    output = output + self.selfID
                    
                    #Pipe ID
                    output = output << 40 
                    output = output + self.read_child[slot]
                    
                    #Depth
                    output = output << 8
                    output = output + self.depth
                    
                    #Depth
                    output = output << 8
                    output = output + self.sensorTime
                    
                    #Add to write child array
                    self.write_child[slot] = talkBack
                    self.check_child[slot] = True
                    
                    #Write to Child with Parent Details
                    self.writeOut(self.write_child[slot],output)    

                    #Add Child to Counter
                    self.numChildren += 1
                    
###########################################################################################################
         

###########################################################################################################
                #///////////////////////////////////////////////////////////////////////
                #// 3 = Message to Master
                #//
                #// Packet Structure = B       BBBB    BBBB    B      BB BB BB BB BB BB BB BB BB BB          F
                #//                    Header  NodeID  ChildID Type   Payload
                #//                                                         Air ~ Ground ~ Moist 1-10 ~ Reserved   End Byte
                #//
                #///////////////////////////////////////////////////////////////////////
                
                #make sure the slot has been taken and its not just a rogue message
                if(header == self.masterInfo and writeSlot != -1):
                    
                    #Get the child nodeID
                    childNodeID = int.from_bytes(message[5:9], byteorder='big', signed=False)
                    
                    #Get the Type
                    
                    #print("Type " + str(type))
                
                    payload = {}

                    index = 9
                    _TimeOffset = int.from_bytes(message[index:index+1], byteorder='big', signed=False)
                    index += 1
                    payload['Air Temp'] = int.from_bytes(message[index:index+1], byteorder='big', signed=False)
                    index += 1
                    payload['Ground Temp'] = int.from_bytes(message[index:index+1], byteorder='big', signed=False)
                    index += 1
                    payload['Humidity'] = int.from_bytes(message[index:index+1], byteorder='big', signed=False)
                    index += 1
                    payload['Voltage'] = int.from_bytes(message[index:index+1], byteorder='big', signed=False)
                    index += 1
                    payload['Moisture'] = int.from_bytes(message[index:index+1], byteorder='big', signed=False)
                    
                                            
                    print("Node " + str(childNodeID) + "    Values " + str(json.dumps(payload)))
                    
                    #update the check status of the child to true
                    self.check_child[writeSlot] = True

                    #set value
                    output = {'NodeID':str(childNodeID),'Values':payload,'Time':_TimeOffset,'Interval':self.sensorTime}
                    self.Node_Values.put(output)

                    
                    #Write Back to Child with Success
                    #-------------------------------------------------------------------------
                    #- 5 = success message
                    #-
                    #- Send the child we got the packet successfully
                    #-
                    #- Packet Structure = B       BBBB    BBBBBBB
                    #-                    Header  NodeID  Time
                    #-
                    #------------------------------------------------------------------------
                    print(int(time.time() * 1000))
                    self.writeOut(self.write_child[writeSlot],((((self.checkChildPathInfo << 32) + self.selfID) << 64) + int(time.time() * 1000)))
                    
###########################################################################################################
            
            #check the the children have responded in the last 15 minutes
            if(self.checkTime < time.time()):
                self.checkTime = time.time() + self.checkInterval
                print("checking children")
                for key,value in enumerate(self.check_child):
                    if(value == False and  self.write_child[key] != ""):
                        #remove child
                        self.numChildren -= 1
                        self.write_child[key] = ""
                        print("freeing child slot")
                    else:
                        #Set status to false
                        self.check_child[key] = False             
                print("checking done")

        
            '''#For loop that syncs the server data to the nodes
            for key,status in self.Node_Status.items():
                if status == "fromServer":
                    print("Status is from Server! %s" %key)
                    #/////////////////////////////////////////////////////////////////////
                    #// & = command from master
                    #//
                    #// Packet Structure = &       212              data_data_data
                    #//                    Type    Child NodeID     Command [up to 21 chars]
                    #//
                    #/////////////////////////////////////////////////////////////////////
                    self.writeToChildren("&"+key+self.Node_Values[key])
                    
                    #Reset Node's action state
                    self.Node_Status[key] = "noAction"'''
        
        
    def getMessage(self):
        out = []
        out = self.radio.read(24)
        out = out.decode("utf-8")
        return out 
    
    #write a message to all the children
    def writeToChildren(self,message):
        for child in self.write_child:
            #make sure there are children
            if(child != ""):
                self.writeOut(child,message)

    #Write to the new child what does not have an address
    def writeToNewChild(self,message):
        return self.writeOut(self.newChildAddr,message)
    
    #Write a message out
    def writeOut(self,address,message):
    
        message = self.intToByeArray(message)
        #print("Sending message: " + str(message))
        
        self.radio.openWritingPipe(address)
        tries = 0
        done = False
        #Try to send the message 15 times before giving up
        while(not done and tries < 15):
            self.radio.stopListening()
            done = self.radio.write(message)
            self.radio.startListening()
            tries += 1
            #print(tries, "True\n\r" if done else "Timeout\n\r")

        if(done):
            return True
        else:
            return False
        
    def intToByeArray(self, n):
        out = bytearray()
        while(n > 0):
            out.insert(0,(n & 255))
            n = n >> 8
        return out
        
