real = True

if(real):
    from RadioMesh import *
from multiprocessing import Process, Manager
import time
import json
import requests
import random
import http.cookiejar
import argparse

actuator = 1
sensor = 2

def Radio_Communicator(Node_Values,Sensor_Time):
    
    if(real):
    
        #Real Radio----------------------------
    
        network = RadioMesh(Node_Values,Sensor_Time)
            
    else:
    
        #Fake Radio----------------------------
        payload = {}
        payload['Air Temp'] = (random.randint(0,100))
        payload['Ground Temp'] = (random.randint(0,100))
        payload['Moisture'] = (random.randint(0,100))
        
        Node_Types['123456'] = sensor
        Node_Values['123456'] = payload
        Node_Status['123456'] = 'toServer'
    
        while True:
            time.sleep(1)
            payload = {}
            payload['Air Temp'] = (random.randint(0,100))
            payload['Ground Temp'] = (random.randint(0,100))
            payload['Moisture'] = (random.randint(0,100))
        
            Node_Types['123456'] = sensor
            Node_Values['123456'] = payload
            Node_Status['123456'] = 'toServer'

    

def Server_Communicator(Node_Values):   
    sleep_time = 1
    #counter = 0
    url = 'http://54.186.32.104/libs/php/PiHandler.php'
    controllerID = '1234'
    toServer = False
    while True:
        time.sleep(sleep_time)
        
        while(Node_Values.qsize() > 0):
            _update = {}
            #send updates to server (this is a weird format for legacy reasons)
            _update['Nodes'] = {"":Node_Values.get()}
           
            try:
                r = requests.post(url, data=json.dumps(_update), timeout=5)
            
                j = r.json()
            
                #get data back from server
                #print(j)
            

            except (requests.exceptions.RequestException, ValueError) as e:
                print(e)
                
                print('Cannot connect to Cloud. Please check internet connection.')
               
                #probably a web server error, so we need to store value in a local DB, so we can pass it on later
                #
                #
                #-------------------------------------------------------
                
                pass
                    

if __name__ == '__main__':
    
    Sensor_Time = 15 * 60
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-m","--sensormin", help="Sensor Interval in Minutes (must be integer)", type=int)
    parser.add_argument("-s","--sensorseconds", help="Sensor Interval in Seconds (will override Minutes)", type=int)
    args = parser.parse_args()
    
    Sensor_Time = (args.sensormin * 60) if args.sensormin else Sensor_Time
    Sensor_Time = (args.sensorseconds) if args.sensorseconds else Sensor_Time
    
    print("Using Sensor Interval of " + str(Sensor_Time/60) + " minutes")
    
    manager = Manager()
    Node_Values = manager.Queue()
    
    RadioListner = Process(target=Radio_Communicator, args=(Node_Values,Sensor_Time,))
    RadioListner.start()
    
    ServerCommunicator = Process(target=Server_Communicator, args=(Node_Values,))
    ServerCommunicator.start()
    
    
    RadioListner.join()
    ServerCommunicator.join()
